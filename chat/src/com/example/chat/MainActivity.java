package com.example.chat;				//client for a server 


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

public class MainActivity extends Activity {
	
	
	 private static final String TAG = "de.tavendo.test1";
//		private boolean connected = false;
		private Button b1;
		private EditText t1,t2;
//		private EditText server_ip;
//		private String serverIpAddress="";
		private StringBuffer str=new StringBuffer();
		private WebSocketConnection mConnection = new WebSocketConnection();

	   

	   @Override
	   public void onCreate(Bundle savedInstanceState) {
		   super.onCreate(savedInstanceState);
		   setContentView(R.layout.activity_main);
			start();
			runWS();
	      
	  }
	  public void runWS(){
		  b1=(Button)findViewById(R.id.button1);
		  t1=(EditText)findViewById(R.id.editText1);
		  t2=(EditText)findViewById(R.id.editText2);
		  str.insert(0,"");
		  b1.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					sendMessage();
				}		
			});
		}
	  public void sendMessage(){
		  str.append("Client says: "+t1.getText().toString()+"\n");
		  mConnection.sendTextMessage(t1.getText().toString());
	  }
	  private void start() {

		    final String wsuri = "ws://192.168.10.110:8080/Chat/EchoAnnotation";

		    try {
		       mConnection.connect(wsuri, new WebSocketHandler() {

		          @Override
		          public void onOpen() {
		             Log.d(TAG, "Status: Connected to " + wsuri);
		             mConnection.sendTextMessage("Hello Server");
		             str.append("Client says: Hello Server \n");
				      t2.setText(str);
		          }

		          @Override
		          public void onTextMessage(String payload) {
		             Log.d(TAG, "Got echo: " + payload);
		             str.append("Server says: " + payload+ "\n");
		             t2.setText(str);
		             
		          }

		          @Override
		          public void onClose(int code, String reason) {
		             Log.d(TAG, "Connection lost.");
		             str.append("WebSocket is closed");
				     t2.setText(str);
		          }
		       });
		    } catch (WebSocketException e) {

		       Log.d(TAG, e.toString());
		    }
		 }
	  @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
}